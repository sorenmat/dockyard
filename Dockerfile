FROM scratch
MAINTAINER Soren Mathiasen <sorenm@mymessages.dk>

#RUN apt-get update && apt-get install -y ca-certificates

ADD dockyard /
CMD ["/dockyard"]
