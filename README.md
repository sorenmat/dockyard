# Dockyard

A simple tool the keeps Kubernetes services with NodePort exposed registered in Consul, with support for
mutual TLS for connecting to Consul

# Usage

```
Usage of ./dockyard:
  -alsologtostderr
    	log to standard error as well as files
  -cacert string
    	CA certificate to verify peer against
  -cert string
    	 Client certificate file
  -consul-address string
    	Consul Server address (default "localhost:8500")
  -key string
    	Private key file name
  -kube-config string
    	Kubernetes Config File
  -log_backtrace_at value
    	when logging hits line file:N, emit a stack trace
  -log_dir string
    	If non-empty, write log files in this directory
  -logtostderr
    	log to standard error instead of files
  -stderrthreshold value
    	logs at or above this threshold go to stderr
  -v value
    	log level for V logs
  -vmodule value
    	comma-separated list of pattern=N settings for file-filtered logging
```

Example:

    ./dockyard -consul-address consul:8500

Will run dockyard and listing to services in the cluster it's been deployed in, and register services in the
consul host and port 8500

Service needs to be NodePort exposed and have the annotation `consul.enabled: true` otherwise they will be ignored.

# Contributing

Start with clicking the star button to make the authors happy :)

Then fork the repository and submit a pull request for whatever change you want to be added to this project.

If you have any questions or issues, just open an issue.

# Author

Soren Mathiasen
