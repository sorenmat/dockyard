package main

import (
	"flag"
	"log"
	"time"

	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/cache"

	"fmt"
	consulapi "github.com/hashicorp/consul/api"
	"k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"net"
)

var (
	kubeCfgFile   = flag.String("kube-config", "", "Kubernetes Config File")
	consulAddress = flag.String("consul-address", "localhost:8500", "Consul Server address")

	cert = flag.String("cert", "", " Client certificate file")
	key  = flag.String("key", "", "Private key file name")
	ca   = flag.String("cacert", "", "CA certificate to verify peer against")
)

// Yard keeps track of the clients to Consul and Kubernetes
type Yard struct {
	consulClient *consulapi.Client
	kubeClient   *kubernetes.Clientset
}

func main() {
	flag.Parse()
	config, err := rest.InClusterConfig()
	if err != nil {
		log.Println("Appears we are not running in a cluster")
		config, err = clientcmd.BuildConfigFromFlags("", *kubeCfgFile)
		if err != nil {
			panic(err.Error())
		}
	} else {
		fmt.Println("Seems like we are running in a Kubernetes cluster!!")
	}

	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}

	kc, err := newConsulClient(*consulAddress, *ca, *cert, *key)
	if err != nil {
		log.Fatal(err)
	}

	yard := Yard{
		consulClient: kc,
		kubeClient:   clientset,
	}

	watchlist := cache.NewListWatchFromClient(clientset.Core().RESTClient(), "services", metav1.NamespaceAll, nil)
	_, controller := cache.NewInformer(
		watchlist,
		&v1.Service{},
		time.Second*0,
		cache.ResourceEventHandlerFuncs{
			AddFunc: func(obj interface{}) {
				yard.newService(obj)
			},
			DeleteFunc: func(obj interface{}) {
				yard.removeService(obj)
			},
			UpdateFunc: func(oldObj, newObj interface{}) {
				yard.updateService(oldObj, newObj)
			},
		},
	)

	stop := make(chan struct{})
	controller.Run(stop)
}

// Get the IP Adresse from all the nodes in the Kubernetes cluster
func (yard *Yard) getNodeAdresses() []string {
	var ips []string
	nodes := yard.kubeClient.CoreV1Client.Nodes()
	n, err := nodes.List(metav1.ListOptions{})
	if err != nil {
		log.Fatal(err)
	}

	for _, v := range n.Items {
		nodeName := v.GetName()
		ipStr := ""
		ip, err := net.ResolveIPAddr("ip", nodeName)
		if err != nil {
			log.Println("Not a valid hostname: ", err)
			log.Println("This should not happen, defaulting back to 127.0.0.1")
			ipStr = "127.0.0.1"
		} else {
			ipStr = ip.String()
		}
		ips = append(ips, ipStr)
	}
	return ips
}

// newService will register a new service in consul when it has been discovered in Kubernetes
// The services needs to be registered in Kubernetes with a NodePort otherwise the service will be skipped
// Also create a TCP service check pining the services NodePort every 5 sec.
func (yard *Yard) newService(obj interface{}) {
	if s, ok := obj.(*v1.Service); ok {
		enabled := s.Annotations["consul.enabled"]
		if enabled != "true" {
			log.Printf("%v doesn't have consul.enabled=true, skipping....\n", s.GetName())
			return
		}
		for _, port := range s.Spec.Ports {
			if port.NodePort != 0 {
				fmt.Printf("Seems like %v has a NodePort\n", s.GetName())

				ips := yard.getNodeAdresses()
				for _, ipStr := range ips {

					service := &consulapi.AgentService{
						Service: s.GetName(),
						Tags:    []string{"kubernetes", "dockyard"},
						Port:    int(port.NodePort),
						Address: ipStr,
					}
					fmt.Println(">>>>>>>>>>>>>>>>>>>", s.Namespace)
					reg := &consulapi.CatalogRegistration{
						Node:    s.Namespace,
						Address: ipStr,
						Service: service,
					}

					asr := &consulapi.AgentServiceRegistration{
						Name:    s.GetName(),
						Tags:    []string{"kubernetes", "dockyard"},
						Address: ipStr,
						Port:    int(port.NodePort),
						Check: &consulapi.AgentServiceCheck{
							Interval: "5s",
							TCP:      fmt.Sprintf("%v:%v", ipStr, port.NodePort),
							Notes:    fmt.Sprintf("Service check for reaching the node in the kubernetes cluster, this is a simple TCP check on %v:%v", ipStr, port.NodePort),
							TTL:      "30s",
						},
					}

					err := yard.consulClient.Agent().ServiceRegister(asr)
					if err != nil {
						log.Println("Unable to register service", asr, err)
					}
					wm, err := yard.consulClient.Catalog().Register(reg, &consulapi.WriteOptions{})
					if err != nil {
						log.Println("Error registering service:", err)
					} else {
						log.Println(wm)
					}
				}
			}
		}
	}
}

// removeService will removed services from Consul when they are removed from Kubernetes
func (yard *Yard) removeService(obj interface{}) {
	if s, ok := obj.(*v1.Service); ok {
		log.Printf("Remove Service %+v\n", s.GetName())
		service := &consulapi.CatalogDeregistration{
			ServiceID: s.GetName(),
			Node:      s.Namespace,
		}

		_, err := yard.consulClient.Catalog().Deregister(service, &consulapi.WriteOptions{})
		if err != nil {
			log.Println("Error deregistering service:", err)
		}
	}
}

// updateService will remove and then re add services in Consul when updated in Kubernetes
func (yard *Yard) updateService(oldObj, obj interface{}) {
	yard.removeService(oldObj)
	yard.newService(obj)
}

// newConsulClient creates a new Consul API Client with TLS if specified in the
func newConsulClient(serverAddress string, cert, key, ca string) (*consulapi.Client, error) {
	config := createConsulConfig(serverAddress, ca, cert, key)
	client, err := consulapi.NewClient(config)
	if err != nil {
		return nil, err
	}
	return client, nil
}
func createConsulConfig(serverAddress string, ca string, cert string, key string) *consulapi.Config {
	config := consulapi.DefaultConfig()
	config.Address = serverAddress
	if ca != "" && cert != "" && key != "" {
		config.TLSConfig = consulapi.TLSConfig{
			Address:  serverAddress,
			CertFile: cert,
			KeyFile:  key,
			CAFile:   ca,
		}
	}
	return config
}
