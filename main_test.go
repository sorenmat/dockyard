package main

import "testing"

func TestClientWithCerts(t *testing.T) {
	config := createConsulConfig("localhost", "ca", "cert", "key")
	if config.TLSConfig.CAFile != "ca" {
		t.Error("Expected CAFile to be ca, but was ", config.TLSConfig.CAFile)
	}
	if config.TLSConfig.CertFile != "cert" {
		t.Error("Expected CertFile to be cert")
	}
	if config.TLSConfig.KeyFile != "key" {
		t.Error("Expected KeyFile to be key")
	}

}
